import java.util.*;
public class Que3 extends ObjectData
{
    int rank;
    void rankStudents()
    {
        rank=1;
        System.out.println("Name            Result          Marks"); 
        studentdetails.stream().sorted(Comparator.comparingInt(StudentData::getMarks).reversed()).forEach(i->
        {
            if(i.getMarks()<50)
            {
                System.out.println(i.getName()+"            Fail            "+i.getMarks()); 
            }
            else
            {
                switch(rank)
                {
                    case 1:
                    System.out.println(i.getName()+"            1st             "+i.getMarks());
                    break;
                    case 2:
                    System.out.println(i.getName()+"            2nd             "+i.getMarks());
                    break;
                    case 3:
                    System.out.println(i.getName()+"            3rd             "+i.getMarks());
                    break;
                    default:
                    System.out.println(i.getName()+"            Pass            "+i.getMarks());

                }
                rank++;

            }
        });
    }
    public static void main(String[] args) 
    {
        Que3 q3=new Que3();
        q3.rankStudents();
    }
    
}
