public class StudentData 
{
    private int id;
    private String name;
    private String class_id;
    private int marks;
    private String gender;
    private int age;
    public StudentData(int id, String name, String class_id, int marks, String gender, int age) {
        this.id = id;
        this.name = name;
        this.class_id = class_id;
        this.marks = marks;
        this.gender = gender;
        this.age = age;
    }
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getClass_id() {
        return class_id;
    }
    public void setClass_id(String class_id) {
        this.class_id = class_id;
    }
    public int getMarks() {
        return marks;
    }
    public void setMarks(int marks) {
        this.marks = marks;
    }
    public String getGender() {
        return gender;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    @Override
    public String toString() {
        return "StudentData [id=" + id + ", name=" + name +", age=" + age + ", class_id=" + class_id + ", gender=" + gender + ",  marks="
                + marks +  "]";
    }   
    
    

    
}
