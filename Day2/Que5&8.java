class Que5 extends ObjectData
{
    void failedStudents()
    {
        studentdetails.stream().filter(i->i.getMarks()<50).forEach(System.out::println);
    }  
    void failedStudentsWithGender(String gender)
    {
        studentdetails.stream().filter(i->i.getMarks()<50).filter(j->j.getGender().equals(gender)).forEach(System.out::println);
    } 
    void failedStudentsWithClass(String class_id)
    {
        studentdetails.stream().filter(i->i.getMarks()<50).filter(j->j.getClass_id().equals(class_id)).forEach(System.out::println);
    } 
    void failedStudentsWithMinAge(int age)
    {
        studentdetails.stream().filter(i->i.getMarks()<50).filter(j->j.getAge()>age).forEach(System.out::println);
    } 
    void failedStudentsWithMaxAge(int age)
    {
        studentdetails.stream().filter(i->i.getMarks()>50).filter(j->j.getAge()<age).forEach(System.out::println);
    }
    void failedStudentsWithCity(String city)
    {
        addressdetails.stream().filter(i->i.getCity().equals(city)).forEach(
            j->studentdetails.stream().filter(k->k.getMarks()<50).filter(l->l.getId()==j.getStudent_id()).forEach(System.out::println));
  }  
    void failedStudentsWithPinCode(int pin_code)
    {
        addressdetails.stream().filter(i->i.getPin_code()==pin_code).forEach(
            j->studentdetails.stream().filter(k->k.getMarks()<50).filter(l->l.getId()==j.getStudent_id()).forEach(System.out::println));
    } 
    public static void main(String[] args) 
    {
        Que5 q5=new Que5();
        //q5.failedStudents();
        //q5.failedStudentsWithGender("M");
        //q5.failedStudentsWithClass("A");
        //q5.failedStudentsWithMaxAge(30);
        //q5.failedStudentsWithMinAge(15);
        q5.failedStudentsWithCity("delhi");
        //q5.failedStudentsWithPinCode(462002);
    }
}
