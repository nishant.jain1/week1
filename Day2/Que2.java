public class Que2 extends ObjectData 
{
    void filterByCity(String city)
    {
        addressdetails.stream().filter(i->i.getCity().equals(city)).forEach(
            j->studentdetails.stream().filter(k->k.getId()==j.getStudent_id()).forEach(System.out::println));
    }
    void filterByCityAndGender(String city,String gender)
    {
        addressdetails.stream().filter(i->i.getCity().equals(city)).forEach(
            j->studentdetails.stream().filter(k->k.getId()==j.getStudent_id()).filter(l->l.getGender().equals(gender)).forEach(System.out::println));
    }
    void filterByCityAndClass(String city,String class_id)
    {
        addressdetails.stream().filter(i->i.getCity().equals(city)).forEach(
            j->studentdetails.stream().filter(k->k.getId()==j.getStudent_id()).filter(l->l.getClass_id().equals(class_id)).forEach(System.out::println));
    }
    void filterByCityAndMinAge(String city,int age)
    {
        addressdetails.stream().filter(i->i.getCity().equals(city)).forEach(
            j->studentdetails.stream().filter(k->k.getId()==j.getStudent_id()).filter(l->l.getAge()>age).forEach(System.out::println));
    }
    void filterByCityAndMaxAge(String city,int age)
    {
        addressdetails.stream().filter(i->i.getCity().equals(city)).forEach(
            j->studentdetails.stream().filter(k->k.getId()==j.getStudent_id()).filter(l->l.getAge()<age).forEach(System.out::println));
    }
    public static void main(String[] args) {
        Que2 q2=new Que2();
        //q2.filterByCity("indore");
        //q2.filterByCityAndGender("indore", "M");
        //q2.filterByCityAndClass("indore", "A");
        //q2.filterByCityAndMaxAge("indore", 30);
        q2.filterByCityAndMinAge("indore", 30);
    }
}
