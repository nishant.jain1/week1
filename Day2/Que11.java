import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class Que11 extends ObjectData 
{
    Scanner sc=new Scanner(System.in);

        void paginated()
        {
            int i=0;
            try
           {
                
                uploadStudent("std.csv");
            }
            catch(Exception e){System.out.println(e);}

            while(i<studentdetails.size())
            {
                for(int j=0;j<9;j++)
                {
                    System.out.println(studentdetails.get(i++));
                }
                System.out.println("prev----------------close--------------next");
                String s=sc.next();
                if(s.equals("close"))break;
                else if(s.equals("next"))continue;
                else if(s.equals("prev"))i=i-18;
            }
        }
        void paginatedByName()
        {
            int i=0;
            try
           {
                
                uploadStudent("std.csv");
            }
            catch(Exception e){System.out.println(e);}
            List<StudentData> studentdetails2=studentdetails.stream().sorted((o1,o2)->o1.getName().compareTo(o2.getName())).collect(Collectors.toList());
            while(i<studentdetails2.size())
            {
                for(int j=0;j<9;j++)
                {
                    System.out.println(studentdetails2.get(i++));
                }
                System.out.println("prev----------------close--------------next");
                String s=sc.next();
                if(s.equals("close"))break;
                else if(s.equals("next"))continue;
                else if(s.equals("prev"))i=i-18;
            }
        }
        void paginatedByMarks()
        {
            int i=0;
            try
           {
                
                uploadStudent("std.csv");
            }
            catch(Exception e){System.out.println(e);}
            List<StudentData> studentdetails2=studentdetails.stream().sorted(Comparator.comparingInt(StudentData::getMarks)).collect(Collectors.toList());
            while(i<studentdetails2.size())
            {
                for(int j=0;j<9;j++)
                {
                    System.out.println(studentdetails2.get(i++));
                }
                System.out.println("prev----------------close--------------next");
                String s=sc.next();
                if(s.equals("close"))break;
                else if(s.equals("next"))continue;
                else if(s.equals("prev"))i=i-18;
            }
        }
        void paginatedByGenderAndMarks(String gender)
        {
            int i=0;
            try
           {
                
                uploadStudent("std.csv");
            }
            catch(Exception e){System.out.println(e);}
            List<StudentData> studentdetails2=studentdetails.stream().filter(stu->stu.getGender().equals(gender)).sorted(Comparator.comparingInt(StudentData::getMarks)).collect(Collectors.toList());
            while(i<studentdetails2.size())
            {
                for(int j=0;j<9;j++)
                {
                    System.out.println(studentdetails2.get(i++));
                }
                System.out.println("prev----------------close--------------next");
                String s=sc.next();
                if(s.equals("close"))break;
                else if(s.equals("next"))continue;
                else if(s.equals("prev"))i=i-18;
            }
        }
       public static void main(String[] args) 
       {
        
           Que11 q11=new Que11();
           //q11.paginated();
           //q11.paginatedByName();
           //q11.paginatedByMarks();
           q11.paginatedByGenderAndMarks("F");
           
       }
}
