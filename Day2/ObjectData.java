import java.util.*;
import java.io.*;

public class ObjectData 
{
    ArrayList<StudentData> studentdetails;
    ArrayList<ClassData> classdetails;
    ArrayList<AddressData> addressdetails;
    FileReader file;
    BufferedReader read;
    String line;

    public void uploadStudent(String s)throws FileNotFoundException,IOException
    {
        file = new FileReader(s);
        read = new BufferedReader(file);
        line = null;
        studentdetails = new ArrayList<>();
        while ((line = read.readLine()) != null) 
        {
            String[] studentData = line.split(",");
            StudentData studentdata = new StudentData(Integer.parseInt(studentData[0]),studentData[1], studentData[2], Integer.parseInt(studentData[3]), studentData[4],Integer.parseInt(studentData[5]));
            studentdetails.add(studentdata);
        }
    }
    public void uploadClass(String s)throws FileNotFoundException,IOException
    {
        file = new FileReader(s);
        read = new BufferedReader(file);
        String line = null;
        classdetails=new ArrayList<>();
        while ((line = read.readLine()) != null) 
        {
            String[] classData = line.split(",");
            ClassData classdata = new ClassData(Integer.parseInt(classData[0]),classData[1]);
            classdetails.add(classdata);
        }
    }
    public void uploadAddress(String s)throws FileNotFoundException,IOException
    {
        file = new FileReader(s);
        read = new BufferedReader(file);
        line = null;
        addressdetails=new ArrayList<>();
        while ((line = read.readLine()) != null) 
        {
            String[] addressData = line.split(",");
            AddressData addressdata = new AddressData(Integer.parseInt(addressData[0]),Integer.parseInt(addressData[1]),addressData[2],Integer.parseInt(addressData[3]));
            addressdetails.add(addressdata);
        }
    }
    {
        try
        {
            this.uploadAddress("address.csv");
            this.uploadClass("class.csv");
            this.uploadStudent("student.csv");
        }
        catch(Exception e){}
    }
   public static void main(String[] args) 
   {
      // ObjectData o;
       
   
    }

}

