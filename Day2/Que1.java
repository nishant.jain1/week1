public class Que1 extends ObjectData
{
    
       void filterByPinCode(int pin_code)
       {  
            addressdetails.stream().filter(i->i.getPin_code()==pin_code).forEach(
                j->studentdetails.stream().filter(k->j.getStudent_id()==k.getId()).forEach(System.out::println));
       }
       void filterByPinCodeAndGender(int pin_code,String gender)
       {  
            addressdetails.stream().filter(i->i.getPin_code()==pin_code).forEach(
                j->studentdetails.stream().filter(k->j.getStudent_id()==k.getId()).filter(l->l.getGender().equals(gender)).forEach(System.out::println));
       }
       void filterByPinCodeAndClass(int pin_code,String class_id)
       {  
            addressdetails.stream().filter(i->i.getPin_code()==pin_code).forEach(
                j->studentdetails.stream().filter(k->j.getStudent_id()==k.getId()).filter(l->l.getClass_id().equals(class_id)).forEach(System.out::println));
       }
       void filterByPinCodeAndMaxAge(int pin_code,int age)
       {  
            addressdetails.stream().filter(i->i.getPin_code()==pin_code).forEach(
                j->studentdetails.stream().filter(k->j.getStudent_id()==k.getId()).filter(l->l.getAge()<age).forEach(System.out::println));
       }
        void filterByPinCodeAndMinAge(int pin_code,int age)
       {  
            addressdetails.stream().filter(i->i.getPin_code()==pin_code).forEach(
                j->studentdetails.stream().filter(k->j.getStudent_id()==k.getId()).filter(l->l.getAge()>age).forEach(System.out::println));
       }
       public static void main(String[] args ) 
       {
           Que1 q1=new Que1();
           //q1.filterByPinCode(482002);
          //q1.filterByPinCodeAndGender(482002, "F");
          //q1.filterByPinCodeAndClass(482002, "D");
          //q1.filterByPinCodeAndMaxAge(482002, 25);
          q1.filterByPinCodeAndMinAge(482002, 20);
       }
      
}
