public class Que6 extends ObjectData 
{
    void filterByClass(String class_id)
    {
        studentdetails.stream().filter(i->i.getClass_id().equals(class_id)).forEach(System.out::println);
    }
    void filterByClassAndGender(String class_id,String gender)
    {
        studentdetails.stream().filter(i->i.getClass_id().equals(class_id)).filter(j->j.getGender().equals(gender)).forEach(System.out::println);
    }
    void filterByClassWithMinAge(String class_id,int age)
    {
        studentdetails.stream().filter(i->i.getClass_id().equals(class_id)).filter(j->j.getAge()>age).forEach(System.out::println);
    }
    void filterByClassWithMaxAge(String class_id,int age)
    {
        studentdetails.stream().filter(i->i.getClass_id().equals(class_id)).filter(j->j.getAge()<age).forEach(System.out::println);
    }
    void filterByClassAndCity(String class_id,String city)
    {
        addressdetails.stream().filter(i->i.getCity().equals(city)).forEach(
            j->studentdetails.stream().filter(k->k.getClass_id().equals(class_id)).filter(l->l.getId()==j.getStudent_id()).forEach(System.out::println));
    }
     void filterByClassAndPinCode(String class_id,int pin_code)
    {
        addressdetails.stream().filter(i->i.getPin_code()==pin_code).forEach(
            j->studentdetails.stream().filter(k->k.getClass_id().equals(class_id)).filter(l->j.getStudent_id()==l.getId()).forEach(System.out::println));
    }
    public static void main(String[] args) {
        Que6 q6=new Que6();
        //q6.filterByClass("A");
        //q6.filterByClassAndGender("A","F");
       // q6.filterByClassWithMinAge("A", 15);
       // q6.filterByClassWithMaxAge("A", 15);
       //q6.filterByClassAndCity("A", "indore");
        q6.filterByClassAndPinCode("A", 452002);

    }
}
