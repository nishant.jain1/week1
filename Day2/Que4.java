public class Que4 extends ObjectData
{
    void passedStudents()
    {
        studentdetails.stream().filter(i->i.getMarks()>50).forEach(System.out::println);
    }  
    void passedStudentsWithGender(String gender)
    {
        studentdetails.stream().filter(i->i.getMarks()>50).filter(j->j.getGender().equals(gender)).forEach(System.out::println);
    } 
    void passedStudentsWithClass(String class_id)
    {
        studentdetails.stream().filter(i->i.getMarks()>50).filter(j->j.getClass_id().equals(class_id)).forEach(System.out::println);
    } 
    void passedStudentsWithMinAge(int age)
    {
        studentdetails.stream().filter(i->i.getMarks()>50).filter(j->j.getAge()>age).forEach(System.out::println);
    } 
    void passedStudentsWithMaxAge(int age)
    {
        studentdetails.stream().filter(i->i.getMarks()>50).filter(j->j.getAge()<age).forEach(System.out::println);
    }
    void passedStudentsWithCity(String city)
    {
        addressdetails.stream().filter(i->i.getCity().equals(city)).forEach(
            j->studentdetails.stream().filter(k->k.getMarks()>50).filter(l->l.getId()==j.getStudent_id()).forEach(System.out::println));
  }  
    void passedStudentsWithPinCode(int pin_code)
    {
        addressdetails.stream().filter(i->i.getPin_code()==pin_code).forEach(
            j->studentdetails.stream().filter(k->k.getMarks()>50).filter(l->l.getId()==j.getStudent_id()).forEach(System.out::println));
    } 
    public static void main(String[] args) 
    {
        Que4 q4=new Que4();
        //q4.passedStudents();
        //q4.passedStudentsWithGender("M");
        //q4.passedStudentsWithClass("A");
        //q4.passedStudentsWithMaxAge(30);
        //q4.passedStudentsWithMinAge(15);
        q4.passedStudentsWithCity("delhi");
        //q4.passedStudentsWithPinCode(462002);
    }
}
