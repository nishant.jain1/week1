import java.util.stream.Collector;
import java.util.stream.Collectors;

class Que9 extends ObjectData
{
    StudentData removedStudent=null;
    void deleteStudent(int student_id)
    {
        
        removedStudent=studentdetails.stream().filter(i->i.getId()==student_id).collect(Collectors.toList()).get(0);
        studentdetails.removeIf(i->i.getId()==student_id);
        if(removedStudent!=null)    
        {
            addressdetails.removeIf(i->i.getStudent_id()==removedStudent.getId());
            StudentData sd= studentdetails.stream().filter(i->i.getClass_id().equals(removedStudent.getClass_id())).collect(Collectors.toList()).get(0);
            if(sd==null)
            {
                classdetails.removeIf(i->i.getName().equals(removedStudent.getClass_id()));
            }
        }
    }
    public static void main(String[] args) 
    {
        Que9 q9=new Que9();
        q9.deleteStudent(5);
        System.out.println(q9.studentdetails);
        System.out.println(q9.addressdetails);
    }
}

