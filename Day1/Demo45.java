/*
Equals()
-> this is a method of object class.
-> Equals method of object class compare object by refrence comparision.
-> we can also use it for content comparision by overriding it.

Hascode()
-> this is also a method of object class
-> this method return integer hashcode value for every object
-> if hashcode of two object is not same then the objects are unequal
-> if hashcode of two object is same then the objects may be equal or unequal
*/

class Demo45
{
    public static void main(String[] args) 
    {
        String s1="FB";
        String s2="Ea";
        String s3="AA";
        String s4="AA";

        System.out.println(s1.hashCode());
        System.out.println(s2.hashCode());
        System.out.println(s3.hashCode());

        System.out.println(s1.equals(s2));
        System.out.println(s2.equals(s3));
        System.out.println(s3.equals(s4));
    }
}