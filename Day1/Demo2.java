/*
Default Constructor
-> Contructor without param is called default constructor
-> if their is no constructor available in any class jvm by default create a default constructor
-> that default constructor calls the constructor of its super class
*/
class A2 
{
    A2()
    {
        System.out.println("This is Default Constructor");
    }
}
class Demo2 
{
    public static void main(String[] args) {
        A2 a=new A2();
    }
}
