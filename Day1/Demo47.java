class A47
{  
    synchronized void show(int n)
    {
        for(int i=1;i<=5;i++)
        {  
            System.out.println(n);  
            try 
            {  
                Thread.sleep(400);  
            }
            catch(Exception e){System.out.println(e);}  
        }  
  
    }  
}  
  
class MyThread1 extends Thread
{  
    A47 a;  
    MyThread1(A47 a)
    {  
        this.a=a;  
    }  
    public void run()   
    {  
        a.show(5);  
    }  
}  
class MyThread2 extends Thread
{  
    A47 a;  
    MyThread2(A47 a)
    {  
        this.a=a;  
    }  
    public void run()
    {  
        a.show(100);  
    }  
}  
  
public class Demo47
{  
    public static void main(String args[])
    {  
        A47 obj = new A47();
        MyThread1 t1=new MyThread1(obj);  
        MyThread2 t2=new MyThread2(obj);  
        t1.start();  
        t2.start();  
    }  
}  