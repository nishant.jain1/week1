/*
Inheritence
-> Acquiring properties of super class in subclass is called inheritence
-> it is one of the main concept of OOPs
-> it is used to create a new class with containing all properties of any existing class
-> Inheritance represents the IS-A relationship(Parant-Child Relationship)



Multiple inheritence
-> To prevent ambiguity java does not support multiple inheritence
-> but by using interface we can archive multiple inheritence partially
*/







class A3
{
    void show()
    {
        System.out.println("Inheritence");
    }
}
class B3 extends A3
{

}
class Demo3 
{
    public static void main(String[] args) 
    {
        B3 b=new B3();
        b.show();
    }
}
