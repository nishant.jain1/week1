/*
public java.lang.Runnable is an interface that is to be implemented 
by a class whose instances are intended to be executed by a thread.
 There are two ways to start a new Thread – Subclass Thread and implement
  Runnable. There is no need of sub-classing Thread when a task can be done 
  by overriding only run() method of Runnable.
*/

/*
In a callable interface that basically throws a checked exception and returns
 some results. This is one of the major differences between the upcoming
 Runnable interface where no value is being returned. In this interface, 
 it simply computes a result else throws an exception if unable to do so.
*/
    

