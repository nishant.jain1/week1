/*
Transient 
-> this keyword is used with feild variable to be avoided by serialization.
-> when JVM reads the transient keyword it ignores the original value of the 
object and instead stores the default value of the object 
*/

import java.io.Serializable;
import java.io.*;

class Employee implements Serializable {
    int id;
    String name;
    transient int age;

    public Employee(int id, String name, int age) {
        this.id = id;
        this.name = name;
        this.age = age;
    }
}

class Demo12 {
    public static void main(String args[]) {
        try {
            Employee e1 = new Employee(101, "Nishant", 21);
            FileOutputStream fout = new FileOutputStream("list.txt");
            ObjectOutputStream out = new ObjectOutputStream(fout);
            out.writeObject(e1);
            out.flush();
            out.close();
            System.out.println("Serialization Done");
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}