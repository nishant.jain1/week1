import java.io.FileNotFoundException;
import java.io.FileReader;

/*
Checked Exception 
-> checked by compiler at compile time
-> either handle the exception or specify by throws keyword
-> eg. IOException,SqlException,FileNotFoundException
Unchecked Exception
-> Not checked by compiler at compile time
-> eg. ArithmaticException,NullPointerException,ArrayIndexOutOfBoundException
*/

class Demo18
{
    public static void main(String[] args)throws FileNotFoundException
    {
        FileReader f = new FileReader("C:\\test\\a.txt");

        try
        {
            System.out.println(10/0);
        }
        catch(ArithmeticException e){}
  
    }
}