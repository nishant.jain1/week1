import java.util.HashMap;

/*
HashMap
-> Hashmap allow data insertion in key value pair
-> it does not allow duplicate keys
-> if we try to add same key it will update the value of previous key
-> for compare is HashMap already contain new key or not hashmap use hashcode and equals method
-> hashmap first compare the hashcode of new key with previouslly inserted keys
-> if no hashcode match it allow to new key and pair insertion
-> if any hashcode match found it use equals method to compare
-> if equals method return false it allow to new key and pair insertion
-> if equals method return true it will update previous key value to new value
*/

class Demo46
{
    public static void main(String[] args) 
    {
        HashMap<String,Integer> h=new HashMap<>();
        h.put("one",1);
        h.put("two",2);
        h.put("one",3);
        h.put("three", 3);

        System.out.println(h);




    }
}