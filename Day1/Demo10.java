import java.lang.reflect.Constructor;

/* 
Marker Interface / Tag Interface
-> Method without any feild or body 
-> deliver runtime type information 
-> pre-defined marker Interface 
    Cloneable
    Serializable
*/

interface InnogentEmployee
{
    
}
class Nishant implements InnogentEmployee
{
    String name;
    int id;
    Nishant(String name,int id)
    {
        this.name=name;
        this.id=id;
    }
    public String toString()
    {
        return "Name : "+name+"\nid : "+id;
    }
}
class Shyam 
{
    String name;
    int id;
    Shyam(String name,int id)
    {
        this.name=name;
        this.id=id;
    }
}
class Demo10
{
    static void enterOffice(InnogentEmployee obj)
    {
        System.out.println(obj);
    }
    public static void main(String[] args) {
        Nishant n=new Nishant("Nishant Jain", 101);
        Shyam s=new Shyam("Shyam Ji", 102);
        enterOffice(n);
        enterOffice(s);
    }
}
