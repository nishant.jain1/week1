/*
Cloneable
-> this interface is used to create the exact copy of an object
-> class must implement the Cloneable interface if we want to create the clone
-> The clone() method of the Object class is used to create the clone of the object.
-> the clone() method generates the CloneNotSupportedException.
*/

class Employee implements Cloneable {
    int id;
    String name;

    Employee(int id, String name) {
        this.id = id;
        this.name = name;
    }

   
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
class Demo13
{
    public static void main(String[] args) {
        Employee e = new Employee(101, "Nishant");
        System.out.println(e.id + " " + e.name);
        try {
            Employee e1 = (Employee) e.clone();
            System.out.println(e1.id + " " + e1.name);
        } catch (Exception ex) {
            System.out.println(e.toString());
        }
    }
}