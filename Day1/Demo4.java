/*
Polymorphism
-> perform single action in multiple ways is polymorphism
-> two types of polymorphism
-> compile time polymorphism/static binding  eg:- Method Hiding
-> runtime polymorphism/dynamic binding   eg:-Method Overriding
*/

class A4
{
    void show()
    {
        System.out.println("Super class");
    }
}
class B4 extends A4
{
    void show()
    {
        System.out.println("This is polymorphism");
    }
}
class Demo4 
{
    public static void main(String[] args) 
    {
        B4 b=new B4();
        b.show();
    }
}