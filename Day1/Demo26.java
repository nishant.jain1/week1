public class Demo26 {
    public static void main(String args[]) {
        String s1 = "abcdcba";
        String s2 = "abcdcba";
        String s3 = "acbcda";

        System.out.println("S1->S2:- " + s1.compareTo(s2));
        
        System.out.println("S2->S3:- " + s2.compareTo(s3));
    }
}