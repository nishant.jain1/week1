/*
-> Declare the class as final so it can't be extended.
-> Make all fields private so that direct access is not allowed.
-> Don't provide setter methods for variables.
-> Make all mutable fields final so that its value can be assigned only once.
*/


final class A  
{    
private final String s;    
A(String s)  
{    
this.s=s;    
}  
String getString(){    
return s;    
}    
}    
class Demo9  
{  
public static void main(String ar[])  
{  
A e = new A("permanent");  
String s = e.getString();  
System.out.println("String is: " + s);  
}  
} 