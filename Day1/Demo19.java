/*
Exception
-> occurs at runtime
-> we can handle exception
-> we can throw mannually
-> superclass throwable
error
-> occurs at both runtime and compiletime
-> we cannot handle it
-> we cannot throw it mannually
-> superclass is throwable
*/
class A19 {
 
    public static void test(int i)
    {
        if (i == 0)
            return;
        else {
            test(i++);
        }
    }
}
class Demo19 {
    public static void main(String[] args)
    {
        A19.test(5);

        try
        {
            System.out.println(10/0);
        }
        catch(ArithmeticException e){}
    }
}
