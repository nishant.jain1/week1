import java.util.ArrayList;

import java.util.*;
class Demo36
{
    public static void main(String[] args) 
    {
        Employee e1=new Employee(101,"Nishant",21,"Male","Product Devlopment",2022,10000.0);  
        Employee e2=new Employee(102,"Ramesh",24,"Male","Product Devlopment",2018,18000.0);  
        Employee e3=new Employee(103,"Sunita",22,"Female","Sales and Marketing",2016,22000.0);  
        Employee e4=new Employee(104,"Bhargav",31,"Male","Product Devlopment",2019,25000.0);  
        Employee e5=new Employee(105,"Chameli",33,"Female","Sales and Marketing",2011,13000.0);  
        Employee e6=new Employee(106,"Pushpa",28,"Male","Sales and Marketing",2012,19000.0);  
        Employee e7=new Employee(107,"Rashmi",26,"Female","Product Devlopment",2017,16500.0);  

        List<Employee> l=new ArrayList<>();
        l.add(e1);
        l.add(e2);
        l.add(e3);
        l.add(e4);
        l.add(e5);
        l.add(e6);
        l.add(e7);

        int employeeInProductDevlopment=0,employeeInSalesAndMarketing=0;

        for(int i=0;i<l.size();i++)
        {
            if(l.get(i).getDepartment().equals("Product Devlopment"))employeeInProductDevlopment++;
            else employeeInSalesAndMarketing++;
        }
        System.out.println("Product Devlopment :"+employeeInProductDevlopment+"\nSales and Marketing :"+employeeInSalesAndMarketing);

        
    }   
}
