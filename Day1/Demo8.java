class Circle 
{
    private double radius;
    private double area;

    public double getArea() {
        return area;
    }
    public double getRadius() {
        return radius;
    }
    public void setArea(double area) {
        this.area = area;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
}
class Demo8
{
    public static void main(String[] args) {
        Circle c=new Circle();
        c.setArea(154);
        c.setRadius(7);
        System.out.println(c.getArea());
        System.out.println(c.getRadius());
    }
}