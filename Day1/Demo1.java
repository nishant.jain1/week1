/*
Constructor 
-> a constructor is a block of codes similar to the method.
-> It is called when an instance of the class is created.
-> It is a special type of method which is used to initialize the object.
-> Every time an object is created using the new() keyword, at least one constructor is called.
*/
class A1 
{
    A1()
    {
        System.out.println("This is Constructor");
    }
}
class Demo1
{
    public static void main(String[] args) 
    {
        A1 a=new A1();   
    }
}