/*
Interface
-> An interface in Java is a blueprint of a class
-> The interface in Java is a mechanism to achieve abstraction
->that interfaces can have abstract methods and variables.
-> It cannot have a method body.
*/

interface inter14
{
    void show();
}
class A14 implements inter14
{
    public void show()
    {
        System.out.println("Interface implemented...");
    }
}
class Demo14
{
    public static void main(String[] args) 
    {
        A14 a=new A14();
        a.show();   
    }
}