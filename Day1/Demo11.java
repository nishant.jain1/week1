/*
Serialization
-> mechanism of writing the state of an object into a byte-stream
-> The reverse operation of serialization is called deserialization
-> For serializing the object, we call the writeObject()
-> for deserialization we call the readObject() 
*/

import java.io.Serializable;  
import java.io.*;  
class Employee implements Serializable
{  
    int id;  
    String name;  
    public Employee(int id, String name) {  
    this.id = id;  
    this.name = name;  
 }  
}  
  
class Demo11
{    
    public static void main(String args[])
    {    
        try
        {     
            Employee e1 =new Employee(101,"Nishant");      
            FileOutputStream fout=new FileOutputStream("list.txt");    
            ObjectOutputStream out=new ObjectOutputStream(fout);    
            out.writeObject(e1);    
            out.flush();       
            out.close();    
            System.out.println("Serialization Done");    
        }
        catch(Exception e)
        {
            System.out.println(e);
        }    
 }    
}   