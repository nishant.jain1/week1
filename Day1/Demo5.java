/*
Overloading
Same class___Same method name______Different in parameter
Overriding
Diff class___same method name______Different in parameter

*/
class A5
{
    void show(int x)
    {
        System.out.println("Single param overloding");
    }
    void show(int x,int y)
    {
        System.out.println("double param overloding");
    }
}
class B5
{
    void show()
    {
        System.out.println("Super class");
    }
}
class C5 extends B5
{
    void show()
    {
        System.out.println("This is overriding");
    }
}
public class Demo5 
{ 
    public static void main(String[] args) {
        
        A5 a=new A5();
        a.show(10); 
        a.show(10,20); 
        C5 c=new C5();
        c.show();
    }
}
