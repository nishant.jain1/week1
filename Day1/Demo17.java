/*
Abstract class over Interface
-> if we want to use common feilds without public and final modifiers in subclasses
-> if we want to use common constructor in subclasses
*/
abstract class A17
{
    String s= "innogent";
    A17()
    {
        System.out.println("Constructor of Abstract Class");
    }
}
class B17 extends A17
{
    void show()
    {
        System.out.println("Nishant is an employee of"+s);
    }
}
class C17 extends A17
{
    void show()
    {
        System.out.println("Rahul is an employee of"+s);
    }
}
class Demo17 
{
    public static void main(String[] args) 
    {
        B17 b=new B17();
        b.show();
        C17 c=new C17();
        c.show();
    }
}
