/*
Abstract class
-> with abstract keyword we can declare any class abstract
-> abstract class can have both abstract and non abstract method
-> we can not instantiate abstract class
-> we have to inherit abstract class for using it 
-> we have to override all abstract method of abstract class in sub class
   else we have to declare sub class abstract
*/
abstract class B15
{
    abstract void show();
}
class A15 extends B15
{
    public void show()
    {
        System.out.println("Interface implemented...");
    }
}
class Demo15
{
    public static void main(String[] args) 
    {
        A15 a=new A15();
        a.show();   
    }
}