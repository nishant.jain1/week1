/*
uses of default keyword
->switch case statement
->inside interface(from java 8)
*/
interface inter16
{
    default void show()
    {
        System.out.println("default method");
    }
}
class A16 implements inter16
{

}
class Demo16
{
    public static void main(String[] args) 
    {
        A16 a=new A16();
        a.show();

        int i=5;
        switch(i)
        {
            case 1:
            System.out.println("1");
            break;
            case 2:
            System.out.println("2");
            break;
            default:
            System.out.println("Default case");
            break;

        }
    }
}