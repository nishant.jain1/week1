/*
Encpsulation
-> wrapping up of data under a single unit.
-> combination of data-hiding and abstraction.
-> It is more defined with setter and getter method
*/
class Circle 
{
    private double radius;
    private double area;

    public double getArea() {
        return area;
    }
    public double getRadius() {
        return radius;
    }
    public void setArea(double area) {
        this.area = area;
    }
    public void setRadius(double radius) {
        this.radius = radius;
    }
}
class Demo7
{
    public static void main(String[] args) {
        Circle c=new Circle();
        c.setArea(154);
        c.setRadius(7);
        System.out.println(c.getArea());
        System.out.println(c.getRadius());
    }
}